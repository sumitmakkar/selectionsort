#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> list;
    
        void swapIndexes(int index1 , int index2)
        {
            int temp     = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }
    
    public:
        Engine(vector<int> l)
        {
            list = l;
        }
    
        void sortList()
        {
            int len = (int)list.size();
            for(int i = 0 ; i < len - 1 ; i++)
            {
                int minIndex = i;
                for(int j = i+1 ; j < len ; j++)
                {
                    if(list[j] < list[minIndex])
                    {
                        minIndex = j;
                    }
                }
                swapIndexes(i , minIndex);
            }
        }
    
        void display()
        {
            int len = (int)list.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<list[i]<<" ";
            }
            cout<<endl;
        }
};

int main(int argc, const char * argv[])
{
    vector<int> listVector = {12 , 35 , 87 , 26 , 9 , 28 , 7};
    Engine e               = Engine(listVector);
    e.sortList();
    e.display();
    return 0;
}
